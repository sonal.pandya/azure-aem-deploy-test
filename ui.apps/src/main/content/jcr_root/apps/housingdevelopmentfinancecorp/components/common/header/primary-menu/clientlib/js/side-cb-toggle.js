(function (document, $) {
  "use strict";

  // when dialog gets injected
  $(document).on("foundation-contentloaded", function (e) {
      // if there is already an inital value make sure the according target element becomes visible
      checkboxShowHideHandler($(".side-cb-toggle", e.target));
      var toggleSideMenu = document.querySelectorAll('.toggleSideMenu')
      toggleSideMenu.forEach(function(item) {
        var coralSideId = item.closest('.coral3-Multifield-item').id
        var coralSideCB = Array.from(item.parentElement.children).find(function(item) {
          return item.classList.contains('side-cb-toggle')
        })
        console.log(coralSideCB);
        coralSideCB.setAttribute('data-cb-toggle-target', '.'+coralSideId+'-side-cb')
        item.classList.remove('toggleSideMenu')
        item.classList.add(coralSideId+'-side-cb')
      })
  });

  $(document).on("change", ".side-cb-toggle", function (e) {
      checkboxShowHideHandler($(this));
  });

  function checkboxShowHideHandler(el) {
      el.each(function (i, element) {
          if($(element).is("coral-checkbox")) {
              // handle Coral3 base drop-down
              Coral.commons.ready(element, function (component) {
                  showHide(component, element);
                  component.on("change", function () {
                      showHide(component, element);
                  });
              });
          } else {
              // handle Coral2 based drop-down
              var component = $(element).data("checkbox");
              if (component) {
                  showHide(component, element);
              }
          }
      })
  }

  function showHide(component, element) {
      // get the selector to find the target elements. its stored as data-.. attribute
      var target = $(element).data("cbToggleTarget");
      var $target = $(target);


      if (target) {
          $target.addClass("hide");
          if (component.checked) {
              $target.removeClass("hide");
          }
      }
  }
})(document, Granite.$);