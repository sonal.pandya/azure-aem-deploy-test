document.addEventListener('DOMContentLoaded', function() {
  var mobileNav = document.querySelector('.mobile-blog-navbar')
  var backdrop = document.querySelector('.backdrop')
  var navOpenBtn = document.querySelector('.mobile-nav-open-btn')
  var navCloseBtn = document.querySelector('.mobile-nav-close-btn')

  navOpenBtn.addEventListener('click', function(e) {
    backdrop.classList.remove('hidden')
    mobileNav.classList.remove('mn-hidden')
    navOpenBtn.classList.add('hidden')
    document.body.style.overflow = 'hidden'
  })

  navCloseBtn.addEventListener('click', function(e) {
    backdrop.classList.add('hidden')
    mobileNav.classList.add('mn-hidden')
    navOpenBtn.classList.remove('hidden')
    document.body.style.overflow = 'unset'
  })

  backdrop.addEventListener('click', function(e) {
    backdrop.classList.add('hidden')
    mobileNav.classList.add('mn-hidden')
    navOpenBtn.classList.remove('hidden')
    document.body.style.overflow = 'unset'
  })

})