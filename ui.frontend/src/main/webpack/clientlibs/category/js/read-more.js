$(function () {
  $('.read-more').on('click', function () {
    $('.read-more-content').slideDown();
    $(this).hide();
  });

  $('.read-less').on('click', function () {
    $('.read-more-content').slideUp();
    $('.read-more').show();
  });

})