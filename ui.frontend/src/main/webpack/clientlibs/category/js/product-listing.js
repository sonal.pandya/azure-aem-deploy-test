$(function(){
    $('.accord-toggle').on('click', function(){
        $(this).parent('.accord-title').next('.accord-content').slideToggle();
        $(this).parent('.accord-title').parent('.accord-div').siblings().children('.accord-content').slideUp();
        $(this).parent('.accord-title').parent('.accord-div').siblings().children('.accord-title').children('.accord-toggle').removeClass('active');
        $(this).toggleClass('active');
    })
})