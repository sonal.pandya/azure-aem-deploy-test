document.addEventListener('DOMContentLoaded', function () {

  var modal = document.querySelector('.loan-modal')
  var backdrop = document.querySelector('.loan-modal-backdrop')
  var getQuoteBtn = document.querySelector('.get-quote-btn')

  function hideModal(modal, backdrop) {
    modal.classList.remove('modal-open')
    document.body.style.overflow = 'unset'
    setTimeout(() => {
      modal.style.display = 'none'
      backdrop.classList.add('backdrop-hidden')
    }, 300);
  }

  function showModal(modal, backdrop) {
    modal.style.display = 'block'
    backdrop.classList.remove('backdrop-hidden')
    document.body.style.overflow = 'hidden'
    setTimeout(function () {
      modal.classList.add('modal-open')
    }, 1)
  }

  backdrop.addEventListener('click', function () {
    hideModal(modal, backdrop)
  })

  getQuoteBtn.addEventListener('click', function (event) {
    event.preventDefault()
    showModal(modal, backdrop)
  })

  // var quickModal = document.querySelector('.quicklinks-popup.know-more')
  // var quickModalCibil = document.querySelector('.quicklinks-popup.cibil-score')
  // var blackBackdrop = document.querySelector('.black-backdrop')
  // var quickCloseBtn = document.getElementById('quickLinksCloseBtn')
  // var quickCloseBtnCibil = document.getElementById('quickLinksCloseBtnCibil')
  // var quickOpenBtn = document.querySelector('.quicklinks-popup-btn .cmp-teaser .cmp-teaser__action-container .cmp-teaser__action-link')
  // var quickOpenBtnCibil = document.querySelector('.quicklinks-popup-btn-cibil .cmp-teaser .cmp-teaser__action-container .cmp-teaser__action-link')

  // quickOpenBtn.addEventListener('click', function(e) {
  //   e.preventDefault()
  //   showModal(quickModal, blackBackdrop)
  // })
  // quickOpenBtnCibil.addEventListener('click', function(e) {
  //   e.preventDefault()
  //   showModal(quickModalCibil, blackBackdrop)
  // })

  // blackBackdrop.addEventListener('click', function () {
  //   hideModal(quickModal, blackBackdrop)
  //   hideModal(quickModalCibil, blackBackdrop)
  // })
  
  // quickCloseBtn.addEventListener('click', function(e) {
  //   e.preventDefault()
  //   hideModal(quickModal, blackBackdrop)
  // })
  // quickCloseBtnCibil.addEventListener('click', function(e) {
  //   e.preventDefault()
  //   hideModal(quickModalCibil, blackBackdrop)
  // })
})


document.addEventListener('DOMContentLoaded', function() {
  var getQuoteIncBtn = document.querySelector('.get-quote-inc')
  var getQuoteDecBtn = document.querySelector('.get-quote-dec')
  var quoteInp = document.getElementById('loan-amt-quote')
  quoteInp.value = Number(quoteInp.value).toLocaleString('en-in')
  getQuoteIncBtn.addEventListener('click', function() {
    quoteInp.value = Number(quoteInp.value.replaceAll(',', '')) + 100000
    var quoteValInc = Number(quoteInp.value)
    quoteInp.value = quoteValInc.toLocaleString('en-in')
  })
  getQuoteDecBtn.addEventListener('click', function() {
    quoteInp.value = Number(quoteInp.value.replaceAll(',', '')) - 100000
    var quoteValDec = Number(quoteInp.value)
    quoteInp.value = quoteValDec.toLocaleString('en-in')
  })
})