document.addEventListener('DOMContentLoaded', function () {

  $(function () {
    if (window.innerWidth < 991) {
      $('.home-loan-product .cmp-container').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        dots: false,
        accessibility: false,

      });
    }
  })
})