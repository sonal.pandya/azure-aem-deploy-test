$(function () {
    $('.banner-slick-init > .cmp-container').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: true,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 5000,
        accessibility: false
    });
})
