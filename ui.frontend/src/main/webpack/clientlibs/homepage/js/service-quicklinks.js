document.addEventListener('DOMContentLoaded', function () {

  $(function () {
    $('.quicklink-slide .cmp-container').slick({
      slidesToShow: 5,
      slidesToScroll: 3,
      arrows: true,
      // centerMode: true,
      dots: false,
      infinite: true,
      accessibility: false,
      responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          }
        },
        // {
        //   breakpoint: 768,
        //   settings: {
        //     slidesToShow: 1,
        //     slidesToScroll: 1,
        //   }
        // },
      ]
    });
  })
})