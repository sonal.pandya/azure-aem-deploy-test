document.addEventListener('DOMContentLoaded', function () {

  $(function () {
    if (window.innerWidth < 991) {
      $('.slick-init .cmp-container').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        dots: false,
        accessibility: false,
      });
    } else {
      $('.slick-init .cmp-container').slick('unslick');
    }
  })
})