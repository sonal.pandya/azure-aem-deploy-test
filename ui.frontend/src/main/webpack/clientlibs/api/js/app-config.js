/*******************************************App Config - Start******************************************************/

(function (_global) {
    var _appConfig = (function (jsHelper) {
        if (exceptionUtility) {
            exceptionUtility.dependencyCheck([jsHelper], "App Config");
        }

        var domainPath = "https://34.139.151.113/klarify/web/api"
        // var domainPath = API_ENDPOINT_URL;
        return jsHelper.freezeObj({
            domainPath: domainPath
        });
    })(_global.jsHelper);

    _global.jsHelper.defineReadOnlyObjProp(_global, 'appConfig', _appConfig);
})(this);

/*******************************************App Config - Start******************************************************/