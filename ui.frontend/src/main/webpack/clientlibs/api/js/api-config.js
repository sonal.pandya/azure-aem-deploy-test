/*******************************************API Config Module - Start******************************************************/
(function(_global) {
    var _apiConfig = (function(jsHelper) {
        if (exceptionUtility) {
            exceptionUtility.dependencyCheck([jsHelper], "API Config");
        }
        /*** API Constant Values ***/

        var DEMO = "DEMO";

        /*** API Constants ***/

        var apiConstants = {};

        apiConstants[DEMO] = DEMO;

        /*** API Selectors ***/

        var apiSelectors = {};

        apiSelectors[DEMO] = "demo";

        /*** API Config Object to expose ***/

        var apiConfig = {};
        Object.keys(apiConstants).forEach(function(eachApiConstant) {
            apiConfig[eachApiConstant] = jsHelper.freezeObj({
                "name": apiConstants[eachApiConstant],
                "selector": apiSelectors[eachApiConstant]
            });
        });
        return jsHelper.freezeObj(apiConfig);
    })(_global.jsHelper);
    _global.jsHelper.defineReadOnlyObjProp(_global, 'apiConfig', _apiConfig);
})(this);