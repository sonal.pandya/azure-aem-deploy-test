/*******************************************API Utility Module - Start******************************************************/
(function (_global) {
    var _apiUtility = (function fnApiUtility(
        jsHelper,
        appConfig,
        apiConfig,
        ajaxUtility
    ) {
        if (exceptionUtility) {
            exceptionUtility.dependencyCheck(
                [jsHelper, appConfig, apiConfig, ajaxUtility],
                "API Utility"
            );
        }
        var apiUtilityObj = {};
        var call = function callApi(eachApiConfig, data) {
            return new Promise(function (resolve, reject) {
                var apiUrl = appConfig.domainPath + eachApiConfig.selector;
                ajaxUtility
                    .postJson(apiUrl, data)
                    .then(function (responseText) {
                        resolve(responseText);
                    })
                    .catch(function (error) {
                        reject(error);
                    });
            });
        };

        var getcall = function callApi(eachApiConfig, data) {
            return new Promise(function (resolve, reject) {
                var apiUrl = appConfig.domainPath + eachApiConfig.selector;
                ajaxUtility
                    .getJson(apiUrl, data)
                    .then(function (responseText) {
                        resolve(responseText);
                    })
                    .catch(function (error) {
                        reject(error);
                    });
            });
        };

        /***********POST DEMO API**********/
        var demoPostApi = function demoPostApi(data) {
            return new Promise(function (resolve, reject) {
                call(apiConfig.DEMO, data)
                    .then(function (response) {
                        if (jsHelper.isStr(response)) {
                            response = jsHelper.parseJson(response);
                        }
                        resolve(response);
                    })
                    .catch(function (err) {
                        reject(err);
                    });
            });
        };
        apiUtilityObj.demoPostApi = demoPostApi;

        /***********POST DEMO API**********/

        /***********GET DEMO API**********/

        var demoGetApi = function (data) {
            return new Promise(function (resolve, reject) {
                getcall(apiConfig.DEMO, data)
                    .then(function (response) {
                        resolve(response);
                    })
                    .catch(function (error) {
                        reject(error);
                    });
            });
        };
        apiUtilityObj.demoGetApi = demoGetApi;

        /***********GET DEMO API**********/

        return jsHelper.freezeObj(apiUtilityObj);

    })(_global.jsHelper, _global.appConfig, _global.apiConfig, _global.ajaxUtility);

    _global.jsHelper.defineReadOnlyObjProp(_global, "apiUtility", _apiUtility);

})(this);
/*******************************************API Utility Module - End******************************************************/