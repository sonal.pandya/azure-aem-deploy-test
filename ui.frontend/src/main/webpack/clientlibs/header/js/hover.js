document.addEventListener('DOMContentLoaded', function () {

  var pmLinks = document.querySelectorAll('.primary-menu-link.dd')

  var hoverLinks = Array.from(pmLinks).map(function (item) {
    return item.getAttribute('id')
  })

  function addHover(link, panel) {
    link.classList.add('pm-hover')
    panel.classList.remove('hidden')
  }

  function removeHover(link, panel) {
    link.classList.remove('pm-hover')
    panel.classList.add('hidden')
  }

  hoverLinks.forEach(function (link) {
    var linkEle = document.getElementById(link)
    var panelEle = document.getElementById(link + '-panel')

    linkEle.addEventListener('mouseover', function () {
      addHover(linkEle, panelEle)
    })

    linkEle.addEventListener('mouseout', function () {
      removeHover(linkEle, panelEle)
    })

    panelEle.addEventListener('mouseover', function () {
      addHover(linkEle, panelEle)
    })

    panelEle.addEventListener('mouseout', function () {
      removeHover(linkEle, panelEle)
    })
  })
})

var header = document.querySelector('.navbar-sticky-top')
var navHeight = document.querySelector('.secondary-navbar').offsetHeight

document.addEventListener('scroll', function (e) {
  if (window.innerWidth > 991) {
    if (window.scrollY > 180) {
      header.style.top = -navHeight + 'px'
      header.style.boxShadow = '0 3px 9px 0 rgb(0 0 0 / 16%)'
    } else {
      header.style.top = '0px'
      header.style.boxShadow = 'none'
    }
  }
})

var ecLink = document.getElementById('ec-link')
var ecMenu = document.getElementById('ec-menu')
var ecMenuVisible = false
document.addEventListener('click', function(e) {

  if (ecMenuVisible && e.target !== ecLink) {
    ecMenuVisible = false
    ecLink.classList.remove('active')
    ecMenu.style.display = 'none'
  } 
})
ecLink.addEventListener('click', function() {
  if (ecMenuVisible) {
    ecMenuVisible = false
  } else {
    ecMenu.style.display = 'block'
    ecMenuVisible = true
  }
  if(ecLink.classList.contains('active')) {
    return
  } else {
    ecLink.classList.add('active')
  }
})
ecLink.addEventListener('mouseover', function() {
  ecLink.classList.add('active')
  ecMenu.style.display = 'block'
})
ecMenu.addEventListener('mouseover', function() {
  ecLink.classList.add('active')
  ecMenu.style.display = 'block'
})
ecLink.addEventListener('mouseout', function() {
  if (!ecMenuVisible) {
    ecLink.classList.remove('active')
    ecMenu.style.display = 'none'
  }
})
ecMenu.addEventListener('mouseout', function() {
  if (!ecMenuVisible) {
    ecLink.classList.remove('active')
    ecMenu.style.display = 'none'
  }
})