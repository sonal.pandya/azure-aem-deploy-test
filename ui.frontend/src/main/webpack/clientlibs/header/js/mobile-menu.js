document.addEventListener("DOMContentLoaded", function (e) {
  var openMenuBtn = document.querySelector('.mn-hamburger-btn')
  var openEcMenuBtn = document.querySelector('.mn-ec-menu-open')
  var mnEcMenuBtn = document.querySelector('#mn-ec-link')
  var mobileEcPopup = document.querySelector('.mn.ec-pop-up-div')
  var closeEcMenuBtn = document.querySelector('.mn.close-ec-btn')
  var mnPrimaryLinks = document.querySelectorAll('.mn-primary-menu-link.dd')

  var menu = new Mmenu("#mn-menu",
    {
      offCanvas: {
        position: "right-front",
      },
      navbar: {
        add: false,
      },
      navbars: [
        {
          position: 'top',
          content: [
            '<div class="mn-menu-header">'+
              '<div class="mn-secondary-navbar">'+
                '<div class="call-us-div">'+
                  '<a href="tel:9289200017" class="call-us-text">+91-9289200017 - Give a Missed Call</a>'+
                '</div>'+
              '</div>'+
              '<div class="mn-header-wrap">'+
                '<div class="mn-logo-select-wrap">'+
                  '<a href="#">'+
                    '<img class="mn-menu-hdfc-logo" src="/content/dam/housingdevelopmentfinancecorp/header/images/hdfc-logo.jpg" alt="HDFC Logo">'+
                  '</a>'+
                  '<div class="secondary-menu-select-wrap">'+
                    '<select name="language-options" id="language-select-options">'+
                      '<option class="language-option" value="english">English</option>'+
                      '<option class="language-option" value="hindi">हिंदी</option>'+
                      '<option class="language-option" value="marathi">मराठी</option>'+
                      '<option class="language-option" value="telugu">తెలుగు</option>'+
                      '<option class="language-option" value="tamil">தமிழ்</option>'+
                      '<option class="language-option" value="kannada">ಕನ್ನಡ</option>'+
                      '<option class="language-option" value="malayalam">മലയാളം</option>'+
                    '</select>'+
                  '</div>'+
                '</div>'+
                '<a class="mn close-hamburger-btn" href="javascript: void(0)">'+
                  '<img class="mn close-hamburger-icon" src="/content/dam/housingdevelopmentfinancecorp/header/icons/close.svg" alt="Close Icon">'+
                '</a>'+
              '</div>'+
            '</div>'
          ],
        }
      ],
    },
    {
      classNames: {
        selected: "active"
      },
    });

  var api = menu.API;

  var closeMenuBtn = document.querySelector('.mn.close-hamburger-btn')
  closeMenuBtn.addEventListener("click", function () {
    api.close();
  });
  
  openMenuBtn.addEventListener("click", function () {
    api.open();
  });

  openEcMenuBtn.addEventListener('click', function() {
    api.close()
    mobileEcPopup.style.transform = 'translateX(0%)'
  })

  mnEcMenuBtn.addEventListener('click', function() {
    mobileEcPopup.style.transform = 'translateX(0%)'
  })
  
  closeEcMenuBtn.addEventListener('click', function() {
    api.open()
    mobileEcPopup.style.transform = 'translateX(-100%)'
  })

  Array.from(mnPrimaryLinks).forEach(function(link) {
    var subMenuLink = link.nextElementSibling.getAttribute('href')
    link.href = subMenuLink;
  })
});