/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 ~ Copyright 2020 Adobe Systems Incorporated
 ~
 ~ Licensed under the Apache License, Version 2.0 (the "License");
 ~ you may not use this file except in compliance with the License.
 ~ You may obtain a copy of the License at
 ~
 ~     http://www.apache.org/licenses/LICENSE-2.0
 ~
 ~ Unless required by applicable law or agreed to in writing, software
 ~ distributed under the License is distributed on an "AS IS" BASIS,
 ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 ~ See the License for the specific language governing permissions and
 ~ limitations under the License.
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

 const path = require('path');

 const BUILD_DIR = path.join(__dirname, 'dist');
 const CLIENTLIB_DIR = path.join(
   __dirname,
   '..',
   'ui.apps',
   'src',
   'main',
   'content',
   'jcr_root',
   'apps',
   'housingdevelopmentfinancecorp',
   'clientlibs'
 );
 
 const libsBaseConfig = {
   allowProxy: true,
   serializationFormat: 'xml',
   cssProcessor: ['default:none', 'min:none'],
   jsProcessor: ['default:none', 'min:none']
 };
 
 // Config for `aem-clientlib-generator`
 module.exports = {
   context: BUILD_DIR,
   clientLibRoot: CLIENTLIB_DIR,
   libs: [
     {
       ...libsBaseConfig,
       name: 'clientlib-dependencies',
       categories: ['hdfchomeloan.dependencies'],
       assets: {
         // Copy entrypoint scripts and stylesheets into the respective ClientLib
         // directories
         js: {
           cwd: 'clientlib-dependencies',
           files: ['**/*.js'],
           flatten: false
         },
         css: {
           cwd: 'clientlib-dependencies',
           files: ['**/*.css'],
           flatten: false
         }
       }
     },
     {
       ...libsBaseConfig,
       name: 'clientlib-site',
       categories: ['hdfchomeloan.site'],
       dependencies: ['hdfchomeloan.dependencies'],
       assets: {
         // Copy entrypoint scripts and stylesheets into the respective ClientLib
         // directories
         js: {
           cwd: 'clientlib-site',
           files: ['**/*.js'],
           flatten: false
         },
         css: {
           cwd: 'clientlib-site',
           files: ['**/*.css'],
           flatten: false
         },
 
         // Copy all other files into the `resources` ClientLib directory
         resources: {
           cwd: 'clientlib-site',
           files: ['**/*.*'],
           flatten: false,
           ignore: ['**/*.js', '**/*.css']
         }
       }
     },
     {
       ...libsBaseConfig,
       name: 'clientlib-api',
       categories: ['hdfchomeloan.api'],
       assets: {
         // Copy entrypoint scripts and stylesheets into the respective ClientLib
         // directories
         js: {
           cwd: 'clientlib-api',
           files: ['**/*.js'],
           flatten: false
         },
         css: {
           cwd: 'clientlib-api',
           files: ['**/*.css'],
           flatten: false
         },
 
         // Copy all other files into the `resources` ClientLib directory
         resources: {
           cwd: 'clientlib-api',
           files: ['**/*.*'],
           flatten: false,
           ignore: ['**/*.js', '**/*.css']
         }
       }
     },
     {
       ...libsBaseConfig,
       name: 'clientlib-stalwart',
       categories: ['hdfchomeloan.stalwart'],
       assets: {
         // Copy entrypoint scripts and stylesheets into the respective ClientLib
         // directories
         js: {
           cwd: 'clientlib-stalwart',
           files: ['**/*.js'],
           flatten: false
         },
         css: {
           cwd: 'clientlib-stalwart',
           files: ['**/*.css'],
           flatten: false
         },
 
         // Copy all other files into the `resources` ClientLib directory
         resources: {
           cwd: 'clientlib-stalwart',
           files: ['**/*.*'],
           flatten: false,
           ignore: ['**/*.js', '**/*.css']
         }
       }
     },
     {
       ...libsBaseConfig,
       name: 'clientlib-base',
       categories: ['hdfchomeloan.base'],
       embed: ['core.wcm.components.accordion.v1', 'core.wcm.components.tabs.v1', 'core.wcm.components.carousel.v1', 'core.wcm.components.image.v2', 'core.wcm.components.breadcrumb.v2', 'core.wcm.components.search.v1', 'core.wcm.components.form.text.v2', 'core.wcm.components.pdfviewer.v1', 'core.wcm.components.commons.datalayer.v1', 'hdfchomeloan.grid'],
       assets: {
         // Copy entrypoint scripts and stylesheets into the respective ClientLib
         // directories
         js: {
           cwd: 'clientlib-base',
           files: ['**/*.js'],
           flatten: false
         },
         css: {
           cwd: 'clientlib-base',
           files: ['**/*.css'],
           flatten: false
         },
 
         // Copy all other files into the `resources` ClientLib directory
         resources: {
           cwd: 'clientlib-base',
           files: ['**/*.*'],
           flatten: false,
           ignore: ['**/*.js', '**/*.css']
         }
       }
     },
     // {
     //   ...libsBaseConfig,
     //   name: 'clientlib-jquery',
     //   categories: ['hdfchomeloan.jquery'],
     //   assets: {
     //     // Copy entrypoint scripts and stylesheets into the respective ClientLib
     //     // directories
     //     js: {
     //       cwd: 'clientlib-jquery',
     //       files: ['**/*.js'],
     //       flatten: false
     //     },
     //     css: {
     //       cwd: 'clientlib-jquery',
     //       files: ['**/*.css'],
     //       flatten: false
     //     },
 
     //     // Copy all other files into the `resources` ClientLib directory
     //     resources: {
     //       cwd: 'clientlib-jquery',
     //       files: ['**/*.*'],
     //       flatten: false,
     //       ignore: ['**/*.js', '**/*.css']
     //     }
     //   }
     // },
     {
       ...libsBaseConfig,
       name: 'clientlib-bootstrap',
       categories: ['hdfchomeloan.bootstrap'],
       assets: {
         // Copy entrypoint scripts and stylesheets into the respective ClientLib
         // directories
         js: {
           cwd: 'clientlib-bootstrap',
           files: ['**/*.js'],
           flatten: false
         },
         css: {
           cwd: 'clientlib-bootstrap',
           files: ['**/*.css'],
           flatten: false
         },
 
         // Copy all other files into the `resources` ClientLib directory
         resources: {
           cwd: 'clientlib-bootstrap',
           files: ['**/*.*'],
           flatten: false,
           ignore: ['**/*.js', '**/*.css']
         }
       }
     },
     {
       ...libsBaseConfig,
       name: 'clientlib-mmenu',
       categories: ['hdfchomeloan.mmenu'],
       assets: {
         // Copy entrypoint scripts and stylesheets into the respective ClientLib
         // directories
         js: {
           cwd: 'clientlib-mmenu',
           files: ['**/*.js'],
           flatten: false
         },
         css: {
           cwd: 'clientlib-mmenu',
           files: ['**/*.css'],
           flatten: false
         },
 
         // Copy all other files into the `resources` ClientLib directory
         resources: {
           cwd: 'clientlib-mmenu',
           files: ['**/*.*'],
           flatten: false,
           ignore: ['**/*.js', '**/*.css']
         }
       }
     },
     {
       ...libsBaseConfig,
       name: 'clientlib-common',
       categories: ['hdfchomeloan.common'],
       embed: ['hdfchomeloan.bootstrap', 'hdfchomeloan.mmenu', 'hdfchomeloan.lozad', 'hdfchomeloan.aos'],
       assets: {
         // Copy entrypoint scripts and stylesheets into the respective ClientLib
         // directories
         js: {
           cwd: 'clientlib-common',
           files: ['**/*.js'],
           flatten: false
         },
         css: {
           cwd: 'clientlib-common',
           files: ['**/*.css'],
           flatten: false
         },
 
         // Copy all other files into the `resources` ClientLib directory
         resources: {
           cwd: 'clientlib-common',
           files: ['**/*.*'],
           flatten: false,
           ignore: ['**/*.js', '**/*.css']
         }
       }
     },
     {
       ...libsBaseConfig,
       name: 'clientlib-footer',
       categories: ['hdfchomeloan.footer'],
       assets: {
         // Copy entrypoint scripts and stylesheets into the respective ClientLib
         // directories
         js: {
           cwd: 'clientlib-footer',
           files: ['**/*.js'],
           flatten: false
         },
         css: {
           cwd: 'clientlib-footer',
           files: ['**/*.css'],
           flatten: false
         },
 
         // Copy all other files into the `resources` ClientLib directory
         resources: {
           cwd: 'clientlib-footer',
           files: ['**/*.*'],
           flatten: false,
           ignore: ['**/*.js', '**/*.css']
         }
       }
     },
     {
       ...libsBaseConfig,
       name: 'clientlib-header',
       categories: ['hdfchomeloan.header'],
       assets: {
         // Copy entrypoint scripts and stylesheets into the respective ClientLib
         // directories
         js: {
           cwd: 'clientlib-header',
           files: ['**/*.js'],
           flatten: false
         },
         css: {
           cwd: 'clientlib-header',
           files: ['**/*.css'],
           flatten: false
         },
 
         // Copy all other files into the `resources` ClientLib directory
         resources: {
           cwd: 'clientlib-header',
           files: ['**/*.*'],
           flatten: false,
           ignore: ['**/*.js', '**/*.css']
         }
       }
     },
     {
       ...libsBaseConfig,
       name: 'clientlib-base-common',
       categories: ['hdfchomeloan.base-common'],
       embed: ['hdfchomeloan.header', 'hdfchomeloan.footer'],
       assets: {
         // Copy entrypoint scripts and stylesheets into the respective ClientLib
         // directories
         js: {
           cwd: 'clientlib-base-common',
           files: ['**/*.js'],
           flatten: false
         },
         css: {
           cwd: 'clientlib-base-common',
           files: ['**/*.css'],
           flatten: false
         },
 
         // Copy all other files into the `resources` ClientLib directory
         resources: {
           cwd: 'clientlib-base-common',
           files: ['**/*.*'],
           flatten: false,
           ignore: ['**/*.js', '**/*.css']
         }
       }
     },
     {
       ...libsBaseConfig,
       name: 'clientlib-blog',
       categories: ['hdfchomeloan.blog-common'],
       embed: ['hdfchomeloan.blog-header', 'hdfchomeloan.blog-nav-list', 'hdfchomeloan.blog-footer', 'hdfchomeloan.blog-color-separator', 'hdfchomeloan.blog-share-section', 'hdfchomeloan.blog-archives', 'hdfchomeloan.blog-calc-list', 'hdfchomeloan.blog-home-finance', 'hdfchomeloan.blog-other-locations', 'hdfchomeloan.blog-reach-us', 'hdfchomeloan.blog-related-posts', 'hdfchomeloan.blog-banner-container', 'hdfchomeloan.blog-feed', 'hdfchomeloan.popular-blogs-accordion', 'hdfchomeloan.popular-blogs-tabs', 'hdfchomeloan.popular-blogs-teaser', 'hdfchomeloan.ymal-teaser'],
       assets: {
         // Copy entrypoint scripts and stylesheets into the respective ClientLib
         // directories
         js: {
           cwd: 'clientlib-blog',
           files: ['**/*.js'],
           flatten: false
         },
         css: {
           cwd: 'clientlib-blog',
           files: ['**/*.css'],
           flatten: false
         },
 
         // Copy all other files into the `resources` ClientLib directory
         resources: {
           cwd: 'clientlib-blog',
           files: ['**/*.*'],
           flatten: false,
           ignore: ['**/*.js', '**/*.css']
         }
       }
     },
     {
       ...libsBaseConfig,
       name: 'clientlib-lozad',
       categories: ['hdfchomeloan.lozad'],
       assets: {
         // Copy entrypoint scripts and stylesheets into the respective ClientLib
         // directories
         js: {
           cwd: 'clientlib-lozad',
           files: ['**/*.js'],
           flatten: false
         },
         css: {
           cwd: 'clientlib-lozad',
           files: ['**/*.css'],
           flatten: false
         },
 
         // Copy all other files into the `resources` ClientLib directory
         resources: {
           cwd: 'clientlib-lozad',
           files: ['**/*.*'],
           flatten: false,
           ignore: ['**/*.js', '**/*.css']
         }
       }
     },
     // {
     //   ...libsBaseConfig,
     //   name: 'clientlib-slick',
     //   categories: ['hdfchomeloan.slick'],
     //   assets: {
     //     // Copy entrypoint scripts and stylesheets into the respective ClientLib
     //     // directories
     //     js: {
     //       cwd: 'clientlib-slick',
     //       files: ['**/*.js'],
     //       flatten: false
     //     },
     //     css: {
     //       cwd: 'clientlib-slick',
     //       files: ['**/*.css'],
     //       flatten: false
     //     },
 
     //     // Copy all other files into the `resources` ClientLib directory
     //     resources: {
     //       cwd: 'clientlib-slick',
     //       files: ['**/*.*'],
     //       flatten: false,
     //       ignore: ['**/*.js', '**/*.css']
     //     }
     //   }
     // },
     {
       ...libsBaseConfig,
       name: 'clientlib-aos',
       categories: ['hdfchomeloan.aos'],
       assets: {
         // Copy entrypoint scripts and stylesheets into the respective ClientLib
         // directories
         js: {
           cwd: 'clientlib-aos',
           files: ['**/*.js'],
           flatten: false
         },
         css: {
           cwd: 'clientlib-aos',
           files: ['**/*.css'],
           flatten: false
         },
 
         // Copy all other files into the `resources` ClientLib directory
         resources: {
           cwd: 'clientlib-aos',
           files: ['**/*.*'],
           flatten: false,
           ignore: ['**/*.js', '**/*.css']
         }
       }
     },
     {
       ...libsBaseConfig,
       name: 'clientlib-homepage',
       categories: ['hdfchomeloan.homepage'],
       assets: {
         // Copy entrypoint scripts and stylesheets into the respective ClientLib
         // directories
         js: {
           cwd: 'clientlib-homepage',
           files: ['**/*.js'],
           flatten: false
         },
         css: {
           cwd: 'clientlib-homepage',
           files: ['**/*.css'],
           flatten: false
         },
 
         // Copy all other files into the `resources` ClientLib directory
         resources: {
           cwd: 'clientlib-homepage',
           files: ['**/*.*'],
           flatten: false,
           ignore: ['**/*.js', '**/*.css']
         }
       }
     },
     {
       ...libsBaseConfig,
       name: 'clientlib-category',
       categories: ['hdfchomeloan.category'],
       assets: {
         // Copy entrypoint scripts and stylesheets into the respective ClientLib
         // directories
         js: {
           cwd: 'clientlib-category',
           files: ['**/*.js'],
           flatten: false
         },
         css: {
           cwd: 'clientlib-category',
           files: ['**/*.css'],
           flatten: false
         },
 
         // Copy all other files into the `resources` ClientLib directory
         resources: {
           cwd: 'clientlib-category',
           files: ['**/*.*'],
           flatten: false,
           ignore: ['**/*.js', '**/*.css']
         }
       }
     },
     {
       ...libsBaseConfig,
       name: 'clientlib-product',
       categories: ['hdfchomeloan.product'],
       assets: {
         // Copy entrypoint scripts and stylesheets into the respective ClientLib
         // directories
         js: {
           cwd: 'clientlib-product',
           files: ['**/*.js'],
           flatten: false
         },
         css: {
           cwd: 'clientlib-product',
           files: ['**/*.css'],
           flatten: false
         },
 
         // Copy all other files into the `resources` ClientLib directory
         resources: {
           cwd: 'clientlib-product',
           files: ['**/*.*'],
           flatten: false,
           ignore: ['**/*.js', '**/*.css']
         }
       }
     },
     {
      ...libsBaseConfig,
      name: 'clientlib-aboutUs',
      categories: ['hdfchomeloan.aboutUs'],
      assets: {
        // Copy entrypoint scripts and stylesheets into the respective ClientLib
        // directories
        js: {
          cwd: 'clientlib-aboutUs',
          files: ['**/*.js'],
          flatten: false
        },
        css: {
          cwd: 'clientlib-aboutUs',
          files: ['**/*.css'],
          flatten: false
        },

        // Copy all other files into the `resources` ClientLib directory
        resources: {
          cwd: 'clientlib-aboutUs',
          files: ['**/*.*'],
          flatten: false,
          ignore: ['**/*.js', '**/*.css']
        }
      }
    }
   ]
 };
 