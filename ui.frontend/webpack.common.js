'use strict';

const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TSConfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
// const ESLintPlugin = require('eslint-webpack-plugin');

const SOURCE_ROOT = __dirname + '/src/main/webpack';

const resolve = {
    extensions: ['.js', '.ts'],
    plugins: [new TSConfigPathsPlugin({
        configFile: './tsconfig.json'
    })]
};

module.exports = {
    resolve: resolve,
    entry: {
        site: SOURCE_ROOT + '/site/main.ts',
        api: SOURCE_ROOT + '/clientlibs/api/main.ts',
        stalwart: SOURCE_ROOT + '/clientlibs/stalwart/main.ts',
        common: SOURCE_ROOT + '/clientlibs/common/main.ts',
        // jquery: SOURCE_ROOT + '/clientlibs/jquery/main.ts',
        bootstrap: SOURCE_ROOT + '/clientlibs/bootstrap/main.ts',
        // slick: SOURCE_ROOT + '/clientlibs/slick/main.ts',
        mmenu: SOURCE_ROOT + '/clientlibs/mmenu/main.ts',
        header: SOURCE_ROOT + '/clientlibs/header/main.ts',
        footer: SOURCE_ROOT + '/clientlibs/footer/main.ts',
        lozad: SOURCE_ROOT + '/clientlibs/lozad/main.ts',
        aos: SOURCE_ROOT + '/clientlibs/aos/main.ts',
        homepage: SOURCE_ROOT + '/clientlibs/homepage/main.ts',
        category: SOURCE_ROOT + '/clientlibs/category/main.ts',
        product: SOURCE_ROOT + '/clientlibs/products/main.ts',
        aboutUs: SOURCE_ROOT + '/clientlibs/aboutUs/main.ts'
    },
    output: {
        // filename: (chunkData) => {
        //     return chunkData.chunk.name === 'dependencies' ? 'clientlib-dependencies/[name].js' : 'clientlib-site/[name].js';
        // },
        // path: path.resolve(__dirname, 'dist')

        filename: (chunkData) => {
          let chunkName = chunkData.chunk.name
          return `clientlib-${chunkName}/[name].js`
        },
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'ts-loader'
                    },
                    {
                        loader: 'glob-import-loader',
                        options: {
                            resolve: resolve
                        }
                    }
                ]
            },
            {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: {
                            url: false
                        }
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            plugins() {
                                return [
                                    require('autoprefixer')
                                ];
                            }
                        }
                    },
                    {
                        loader: 'sass-loader',
                    },
                    {
                        loader: 'glob-import-loader',
                        options: {
                            resolve: resolve
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin(),
        // new ESLintPlugin({
        //     extensions: ['js', 'ts', 'tsx']
        // }),
        new MiniCssExtractPlugin({
            filename: 'clientlib-[name]/[name].css'
        }),
        new CopyWebpackPlugin({
            patterns: [
                { from: path.resolve(__dirname, SOURCE_ROOT + '/resources'), to: './clientlib-site/' }
            ]
        })
    ],
    stats: {
        assetsSort: 'chunks',
        builtAt: true,
        children: false,
        chunkGroups: true,
        chunkOrigins: true,
        colors: false,
        errors: true,
        errorDetails: true,
        env: true,
        modules: false,
        performance: true,
        providedExports: false,
        source: false,
        warnings: true
    }
};
