package com.housingdevelopmentfinancecorp.core.services;

/**
 * @author Naresh.Gounder
 *
 */

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;

public interface ResourceHelper {
    public ResourceResolver getResourceResolver() throws LoginException;
}
