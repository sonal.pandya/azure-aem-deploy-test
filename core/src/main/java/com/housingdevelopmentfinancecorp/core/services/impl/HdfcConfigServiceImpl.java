package com.housingdevelopmentfinancecorp.core.services.impl;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@Designate(ocd = HdfcConfigServiceImpl.Config.class)
@Component(service = Runnable.class, immediate = true)
public class HdfcConfigServiceImpl implements Runnable {

    @ObjectClassDefinition(name = "HDFC Ltd Configurations")
    public static @interface Config {
        @AttributeDefinition(name="API EndPoint",description = "Change the API EndPoint here if required")
        public String apiEndPoint() default "";

        @AttributeDefinition(name="Google Analytic",description = "Change the Google Analytic here if required")
        public String googleAnalytic() default "";

        @AttributeDefinition(name="Google Tag Manager",description = "Change the Google Tag Manager here if required")
        public String googleTagManager() default "";

        @AttributeDefinition(name="Notify Visitor",description = "Change the Notify Visitor here if required")
        public String notifyVisitor() default "";

        @AttributeDefinition(name="Chat Bot",description = "Change the Chat Bot here if required")
        public String chatBot() default "";

    }

    @Override
    public void run() {

    }
    public String apiEndPoint;
    public String googleAnalytic;
    public String googleTagManager;
    public String notifyVisitor;
    public String chatBot;


    @Activate
    @Modified
    public void activate(Config config) {
        apiEndPoint = config.apiEndPoint();
        googleAnalytic = config.googleAnalytic();
        googleTagManager=config.googleTagManager();
        notifyVisitor=config.notifyVisitor();
        chatBot=config.chatBot();
    }

}