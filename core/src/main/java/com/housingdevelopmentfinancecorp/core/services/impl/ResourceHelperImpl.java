/**
 * 
 */
package com.housingdevelopmentfinancecorp.core.services.impl;

/**
 * @author Naresh.Gounder
 *
 */
import java.util.HashMap;
import java.util.Map;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.housingdevelopmentfinancecorp.core.constants.UserManagementConstants;
import com.housingdevelopmentfinancecorp.core.services.HdfcHomeLoanLoggerService;
import com.housingdevelopmentfinancecorp.core.services.ResourceHelper;

@Component(service = ResourceHelper.class, immediate = true)
public class ResourceHelperImpl implements ResourceHelper {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Reference
	HdfcHomeLoanLoggerService hdfcHomeLoanLoggerService;

	@Reference
	ResourceResolverFactory resResolverFactory;

	@Override
	public ResourceResolver getResourceResolver() throws LoginException {

		logger.debug("ResourceHelperImpl getResourceResolver method starts :");
		try {
			Map<String, Object> map = new HashMap<String, Object>();

			map.put(ResourceResolverFactory.SUBSERVICE, UserManagementConstants.SUB_SERVICE);
			System.out.println("Map1:"+map);
			ResourceResolver resourceResolver = resResolverFactory.getServiceResourceResolver(map);
			System.out.println("resourceResolver:"+resourceResolver);
			StringBuilder stringBuilderInfo = new StringBuilder(100);
			stringBuilderInfo.append("resourceResolver");
			stringBuilderInfo.append(resourceResolver);
			String logInnfo = stringBuilderInfo.toString();
			logger.debug(logInnfo);
			return resourceResolver;
		} catch (Exception e) {
			logger.error("Exception :" + e.toString());
			hdfcHomeLoanLoggerService.loggerException(e);
		}
		logger.debug("ResourceHelperImpl getResourceResolver method ends :");
		return null;
	}

}
