package com.housingdevelopmentfinancecorp.core.services.impl;

/**
 * @author Naresh.Gounder
 *
 */

import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.housingdevelopmentfinancecorp.core.services.HdfcHomeLoanLoggerService;

@Component(service = HdfcHomeLoanLoggerService.class, immediate = true)
public class HdfcHomeLoanLoggerServiceImpl implements HdfcHomeLoanLoggerService {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public void loggerException(Exception e) {
		StringBuilder stringBuilder = new StringBuilder(100);
		stringBuilder.append("Exception ->{} ");
		stringBuilder.append(e.getClass().getSimpleName());
		stringBuilder.append(": ");
		stringBuilder.append(e);
		String logError = stringBuilder.toString();
		logger.error(logError);
	}

}