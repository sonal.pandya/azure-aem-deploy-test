package com.housingdevelopmentfinancecorp.core.services.impl;

import com.housingdevelopmentfinancecorp.core.config.GlobalCollection;
import com.housingdevelopmentfinancecorp.core.services.CAConfigService;
import com.housingdevelopmentfinancecorp.core.services.UrlRewriterService;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import java.util.Collection;
import java.util.Iterator;

@Component(service = UrlRewriterService.class,immediate = true)
public class UrlRewriterServiceImpl implements UrlRewriterService {


    @Reference
    CAConfigService configService;


    @Override
    public String rewriteUrl(String responseBody, Resource resource, SlingHttpServletRequest request)
    {
        String rewroteContent=responseBody;
        Collection configDatas = configService.getConfigData(request, resource);
        if(responseBody.contains("%$$"))
       {
           Iterator sampleComfigIterator = configDatas.iterator();
           while (sampleComfigIterator.hasNext())
           {
               GlobalCollection configData = (GlobalCollection) sampleComfigIterator.next();

                if(responseBody.contains("%$$" + configData.key() + "$$%"))
                {
                    responseBody =responseBody.replace("%$$" + configData.key() + "$$%",configData.value());

                }
           }
           return  responseBody;

       }
        return rewroteContent;
    }
}
