package com.housingdevelopmentfinancecorp.core.services;

import com.housingdevelopmentfinancecorp.core.models.TestimonialModel;

import java.util.List;

public interface TestimonialDetailsService {
    List<TestimonialModel> getTestimonialDetailsService(String path, ResourceHelper resourceHelper);
}
