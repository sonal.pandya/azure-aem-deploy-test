package com.housingdevelopmentfinancecorp.core.services;

public interface HdfcHomeLoanLoggerService {

	void loggerException(Exception e);

}
