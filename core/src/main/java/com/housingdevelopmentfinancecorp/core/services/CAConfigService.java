package com.housingdevelopmentfinancecorp.core.services;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import java.util.Collection;

public interface CAConfigService {
    Collection getConfigData(SlingHttpServletRequest request, Resource resource);
}
