package com.housingdevelopmentfinancecorp.core.services.impl;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Naresh.Gounder
 *
 */

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.housingdevelopmentfinancecorp.core.constants.HdfcHomeLoanConstants;
import com.housingdevelopmentfinancecorp.core.models.HeaderMenuModel;
import com.housingdevelopmentfinancecorp.core.services.HdfcHomeLoanLoggerService;
import com.housingdevelopmentfinancecorp.core.services.HeaderMenuDetailsService;
import com.housingdevelopmentfinancecorp.core.services.ResourceHelper;

@Component(service = HeaderMenuDetailsService.class, immediate = true)
public class HeaderMenuDetailsServiceImpl implements HeaderMenuDetailsService {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Reference
	ResourceHelper resourceHelper;

	@Reference
	HdfcHomeLoanLoggerService hdfcHomeLoanLoggerService;

	@Override
	public List<HeaderMenuModel> getHeaderLoanProductDetails(String searchPath, ResourceHelper resourceHelper) {

		logger.debug("HeaderMenuDetailsServiceImpl getHeaderLoanProductDetails method starts :");
		List<HeaderMenuModel> headerLoanProductList = new ArrayList<>();

		if (!searchPath.equals("")) {
			Session session = null;
			ResourceResolver resourceResolver = null;

			try {
				resourceResolver = resourceHelper.getResourceResolver();
				session = resourceResolver.adaptTo(Session.class);
				QueryManager queryManager = session.getWorkspace().getQueryManager();
				StringBuilder queryStringBuilder = new StringBuilder();

				queryStringBuilder.append("SELECT * FROM [dam:Asset] AS s WHERE ISDESCENDANTNODE([" + searchPath + "]) order by s.[jcr:created]");
				logger.debug("SQL QUERY - " + queryStringBuilder.toString());

				Query query = queryManager.createQuery(queryStringBuilder.toString(), Query.JCR_SQL2);

				QueryResult queryResult = query.execute();
				NodeIterator nodeIterator = queryResult.getNodes();
				long size = nodeIterator.getSize();
				System.out.println("Size:"+size);

				while (nodeIterator.hasNext()) {

					Node node = nodeIterator.nextNode();
					Node cfnode = resourceResolver.getResource(node.getPath() + "/jcr:content/data/master").adaptTo(Node.class);
					HeaderMenuModel headerLoanProductModel = new HeaderMenuModel();

					if (cfnode.hasProperty(HdfcHomeLoanConstants.LIST_TITLE)) {
						headerLoanProductModel.setListTitle(StringEscapeUtils.escapeHtml(cfnode.getProperty(HdfcHomeLoanConstants.LIST_TITLE).getString()));
					} else {
						headerLoanProductModel.setListTitle("");
					}
					
					if (cfnode.hasProperty(HdfcHomeLoanConstants.ICON_PATH)) {
						headerLoanProductModel.setIconPath(StringEscapeUtils.escapeHtml(cfnode.getProperty(HdfcHomeLoanConstants.ICON_PATH).getString()));
					} else {
						headerLoanProductModel.setIconPath("");
					}
					
					if (cfnode.hasProperty(HdfcHomeLoanConstants.LIST_TITLE_LINK)) {
						headerLoanProductModel.setListTitleLink(StringEscapeUtils.escapeHtml(cfnode.getProperty(HdfcHomeLoanConstants.LIST_TITLE_LINK).getString()));
					} else {
						headerLoanProductModel.setListTitleLink("");
					}
					
					if (cfnode.hasProperty(HdfcHomeLoanConstants.LIST_ITEMS)) {

						Value[] listItemvalues = cfnode.getProperty(HdfcHomeLoanConstants.LIST_ITEMS).getValues();
						List<String> itemValuesList = new ArrayList<>();
						for (int i = 0; i < listItemvalues.length; i++) {
							itemValuesList.add(listItemvalues[i].getString().replace("<p>", "").replace("</p>", "").replace("<br>", ""));
						}
						headerLoanProductModel.setListItems(itemValuesList);
					} else {
						headerLoanProductModel.setListItems(null);
					}

					if (cfnode.hasProperty(HdfcHomeLoanConstants.ICON_ALT_TEXT)) {
						headerLoanProductModel.setIconAltText(StringEscapeUtils.escapeHtml(cfnode.getProperty(HdfcHomeLoanConstants.ICON_ALT_TEXT).getString()));
					} else {
						headerLoanProductModel.setIconAltText("");
					}
					
					if (cfnode.hasProperty(HdfcHomeLoanConstants.LIST_COLUMNS)) {
						headerLoanProductModel.setListColumns(StringEscapeUtils.escapeHtml(cfnode.getProperty(HdfcHomeLoanConstants.LIST_COLUMNS).getString()));
					} else {
						headerLoanProductModel.setListColumns("");
					}

					if(cfnode.hasProperty(HdfcHomeLoanConstants.SECONDARY_MENU_ITEM)){
						Value[] secondaryMenuValue = cfnode.getProperty(HdfcHomeLoanConstants.SECONDARY_MENU_ITEM).getValues();
						List<String> secondaryMenuList =new ArrayList<>();
						for (int i = 0;i < secondaryMenuValue.length;i++){
							secondaryMenuList.add(secondaryMenuValue[i].getString().replace("<p>","").replace("</p>","").replace("<br>",""));
						}
						headerLoanProductModel.setSecondaryMenuItem(secondaryMenuList);
					}
					else {
						headerLoanProductModel.setSecondaryMenuItem(null);
					}



					headerLoanProductList.add(headerLoanProductModel);
				}

			} catch (Exception e) {
				logger.error("Exception :" + e.toString());
				hdfcHomeLoanLoggerService.loggerException(e);

			} finally {
				if (resourceResolver != null) {
					if (resourceResolver.isLive())
						resourceResolver.close();
				}
				if (session != null) {
					if (session.isLive())
						session.logout();
				}

			}
		}
		
		logger.debug("headerLoanProductList size -" + headerLoanProductList.size());
		logger.debug("HeaderMenuDetailsServiceImpl getHeaderLoanProductDetails method starts :");
		return headerLoanProductList;

	}

}
