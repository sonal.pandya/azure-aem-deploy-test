package com.housingdevelopmentfinancecorp.core.services.impl;

import com.housingdevelopmentfinancecorp.core.config.GlobalCollection;
import com.housingdevelopmentfinancecorp.core.services.CAConfigService;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.caconfig.ConfigurationBuilder;
import org.osgi.service.component.annotations.Component;

import java.util.Collection;

@Component(service = CAConfigService.class,immediate = true)
public class CAConfigServiceImpl implements CAConfigService{
    @Override
    public Collection getConfigData(SlingHttpServletRequest request, Resource resource) {
        ConfigurationBuilder configurationBuilder = resource.adaptTo(ConfigurationBuilder.class);
        Collection collection = configurationBuilder.asCollection(GlobalCollection.class);
        return collection;
    }
}
