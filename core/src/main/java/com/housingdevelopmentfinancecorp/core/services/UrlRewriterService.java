package com.housingdevelopmentfinancecorp.core.services;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;


public interface UrlRewriterService {


    public String rewriteUrl(String responseBody, Resource resource, SlingHttpServletRequest request);
}
