/**
 * 
 */
package com.housingdevelopmentfinancecorp.core.services;

import java.util.List;

import com.housingdevelopmentfinancecorp.core.models.HeaderMenuModel;

/**
 * @author Naresh.Gounder
 *
 */
public interface HeaderMenuDetailsService {

	List<HeaderMenuModel> getHeaderLoanProductDetails(String searchpath, ResourceHelper resourceHelper);

}
