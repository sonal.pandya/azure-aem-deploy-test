package com.housingdevelopmentfinancecorp.core.services.impl;

import com.housingdevelopmentfinancecorp.core.constants.HdfcHomeLoanConstants;
import com.housingdevelopmentfinancecorp.core.models.TestimonialModel;
import com.housingdevelopmentfinancecorp.core.services.HdfcHomeLoanLoggerService;
import com.housingdevelopmentfinancecorp.core.services.ResourceHelper;
import com.housingdevelopmentfinancecorp.core.services.TestimonialDetailsService;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Session;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;
import java.util.ArrayList;
import java.util.List;


@Component(service = TestimonialDetailsService.class,immediate = true)
public class TestimonialDetailsServiceImpl implements TestimonialDetailsService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Reference
    ResourceHelper resourceHelper;

    @Reference
    HdfcHomeLoanLoggerService loanLoggerService;

    @Override
    public List<TestimonialModel> getTestimonialDetailsService(String searchPath, ResourceHelper resourceHelper) {
        logger.debug("TestimonialDetailsServiceImpl getHeaderLoanProductDetails method starts :");
        List<TestimonialModel> testimonialModelsList=new ArrayList<>();
        Session session=null;
        ResourceResolver resourceResolver=null;
        try{
            resourceResolver=resourceHelper.getResourceResolver();
            session=resourceResolver.adaptTo(Session.class);
            
            QueryManager queryManager=session.getWorkspace().getQueryManager();
            StringBuffer queryBuffer=new StringBuffer();
            
            queryBuffer.append("SELECT * FROM [dam:Asset] AS s WHERE ISDESCENDANTNODE(["+searchPath+"]) order by s.[jcr:created] desc");
            logger.debug("SQL QUERY - " + queryBuffer.toString());
            
            Query query=queryManager.createQuery(queryBuffer.toString(), Query.JCR_SQL2);
            QueryResult queryResult=query.execute();
            NodeIterator nodeIterator=queryResult.getNodes();
            
            while (nodeIterator.hasNext()) {
                Node node = nodeIterator.nextNode();
                Node nodeData = resourceResolver.getResource(node.getPath() + "/jcr:content/data/master").adaptTo(Node.class);
                TestimonialModel testimonialModel = new TestimonialModel();
                if (nodeData.hasProperty(HdfcHomeLoanConstants.TITLE)) {
                    testimonialModel.setTitle(StringEscapeUtils.escapeHtml(nodeData.getProperty(HdfcHomeLoanConstants.TITLE).getString()));
                } else {
                    testimonialModel.setTitle("");
                }
                if (nodeData.hasProperty(HdfcHomeLoanConstants.DESCRIPTION)) {
					testimonialModel.setDescription(
							StringEscapeUtils.escapeHtml(nodeData.getProperty(HdfcHomeLoanConstants.DESCRIPTION)
									.getString().replace("<p>", "").replace("</p>", "").replace("&amp;", "&")));
                } else {
                    testimonialModel.setDescription("");
                }
                if (nodeData.hasProperty(HdfcHomeLoanConstants.IMAGE_PATH)) {
                    testimonialModel.setImagePath(StringEscapeUtils.escapeHtml(nodeData.getProperty(HdfcHomeLoanConstants.IMAGE_PATH).getString()));
                } else {
                    testimonialModel.setImagePath("");
                }
                if (nodeData.hasProperty(HdfcHomeLoanConstants.ALT_TITLE)) {
                    testimonialModel.setAltTitle(StringEscapeUtils.escapeHtml(nodeData.getProperty(HdfcHomeLoanConstants.ALT_TITLE).getString()));
                } else {
                    testimonialModel.setAltTitle("");
                }
                if (nodeData.hasProperty(HdfcHomeLoanConstants.IMAGE_ALT_TEXT)) {
                    testimonialModel.setImageAltText(StringEscapeUtils.escapeHtml(nodeData.getProperty(HdfcHomeLoanConstants.IMAGE_ALT_TEXT).getString()));
                } else {
                    testimonialModel.setImageAltText("");
                }


                testimonialModelsList.add(testimonialModel);
            }
        }catch (Exception e){
            logger.debug("TestimonialDetailsImpl :-"+e);
        }
        logger.debug("testimonialModelsList size -" + testimonialModelsList.size());
        logger.debug("TestimonialDetailsServiceImpl getHeaderLoanProductDetails method starts :");
        return testimonialModelsList;
    }
}
