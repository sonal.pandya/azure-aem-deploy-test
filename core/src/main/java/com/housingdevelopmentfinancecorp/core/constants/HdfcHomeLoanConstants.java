package com.housingdevelopmentfinancecorp.core.constants;

/**
 * @author Naresh.Gounder
 *
 */
public class HdfcHomeLoanConstants {

	// Page Properties
	public static final String LIST_TITLE = "listTitle";
	public static final String ICON_PATH = "iconPath";
	public static final String LIST_TITLE_LINK = "listTitleLink";
	public static final String LIST_ITEMS = "listItems";
	public static final String ICON_ALT_TEXT = "iconAltText";
	public static final String LIST_COLUMNS = "listColumns";
	public static final String SECONDARY_MENU_ITEM = "secondaryMenuItem";
	public static final String TITLE="title";
	public static final String DESCRIPTION="description";
	public static final String IMAGE_PATH="imagePath";
	public static final String ALT_TITLE="altTitle";
	public static final String IMAGE_ALT_TEXT="imageAltText";

}
