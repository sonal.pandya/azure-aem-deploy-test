package com.housingdevelopmentfinancecorp.core.models.impl;

import com.adobe.cq.wcm.core.components.commons.link.Link;
import com.adobe.cq.wcm.core.components.models.Image;
import com.adobe.cq.wcm.core.components.models.ListItem;
import com.adobe.cq.wcm.core.components.models.Teaser;
import com.housingdevelopmentfinancecorp.core.models.ApplyLoanTeaserModel;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.apache.sling.models.factory.ModelFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;
import java.util.Map;

@Model(adapters = ApplyLoanTeaserModel.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        adaptables = SlingHttpServletRequest.class,
        resourceType = ApplyLoanTeaserModelImpl.RESOURCE_TYPE
)
public class ApplyLoanTeaserModelImpl implements ApplyLoanTeaserModel {
    private Teaser teaser;
    private Image image;
    protected static final String RESOURCE_TYPE = "housingdevelopmentfinancecorp/components/homepage/apply-loan-teaser";
    @Inject
    Resource resource;

    @OSGiService
    private ModelFactory modelFactory;

    @Self
    private SlingHttpServletRequest request;

    @ValueMapValue
    private String mobileImageFile;

    public void setMobileImageFile(String mobileImageFile){
        this.mobileImageFile = mobileImageFile;
    }

    @Override
    public String getDemo() {
        return getDemo();
    }

    @Override
    public List<Map<String, String>> getStats() {
        return getStats();
    }

    @Override
    public boolean isActionsEnabled() {
        return teaser.isActionsEnabled();
    }

    @Override
    public List<ListItem> getActions() {
        return teaser.getActions();
    }

    @Override
    public Link getLink() {
        return teaser.getLink();
    }

    @Override
    public String getLinkURL() {
        return teaser.getLinkURL();
    }

    @Override
    public boolean isImageLinkHidden() {
        return teaser.isImageLinkHidden();
    }

    @Override
    public String getPretitle() {
        return teaser.getPretitle();
    }

    @Override
    public String getTitle() {
        return teaser.getTitle();
    }

    @Override
    public boolean isTitleLinkHidden() {
        return teaser.isImageLinkHidden();
    }

    @Override
    public String getDescription() {
        return teaser.getDescription();
    }

    @Override
    public String getTitleType() {
        return teaser.getTitleType();
    }

    @Override
    public String getMobileImageFile() {
        return mobileImageFile;
    }


    private Image getImage() {
        return image;
    }

    private Teaser getTeaser() {
        return teaser;
    }

    @PostConstruct
    void init() {
        teaser = modelFactory.getModelFromWrappedRequest(request,
                request.getResource(), Teaser.class);
        image = modelFactory.getModelFromWrappedRequest(request,
                request.getResource(),
                Image.class);

    }
}
