package com.housingdevelopmentfinancecorp.core.models;

import java.util.List;

/**
 * @author Naresh.Gounder
 *
 */
public class HeaderMenuModel {
	
	private String listTitle;
	private String iconPath;
	private String listTitleLink;
	private List<String> listItems;
	private String iconAltText;
	private String listColumns;
	private List<String> secondaryMenuItem;
	
	/**
	 * @return the listTitle
	 */
	public String getListTitle() {
		return listTitle;
	}
	
	/**
	 * @param listTitle the listTitle to set
	 */
	public void setListTitle(String listTitle) {
		this.listTitle = listTitle;
	}
	
	/**
	 * @return the iconPath
	 */
	public String getIconPath() {
		return iconPath;
	}
	
	/**
	 * @param iconPath the iconPath to set
	 */
	public void setIconPath(String iconPath) {
		this.iconPath = iconPath;
	}
	
	/**
	 * @return the listTitleLink
	 */
	public String getListTitleLink() {
		return listTitleLink;
	}
	
	/**
	 * @param listTitleLink the listTitleLink to set
	 */
	public void setListTitleLink(String listTitleLink) {
		this.listTitleLink = listTitleLink;
	}
	
	/**
	 * @return the listItems
	 */
	public List<String> getListItems() {
		return listItems;
	}
	
	/**
	 * @param listItems the listItems to set
	 */
	public void setListItems(List<String> listItems) {
		this.listItems = listItems;
	}

	/**
	 * @return the iconAltText
	 */
	public String getIconAltText() {
		return iconAltText;
	}

	public List<String> getSecondaryMenuItem() {
		return secondaryMenuItem;
	}

	public void setSecondaryMenuItem(List<String> secondaryMenuItem) {
		this.secondaryMenuItem = secondaryMenuItem;
	}

	/**
	 * @param iconAltText the iconAltText to set
	 */
	public void setIconAltText(String iconAltText) {
		this.iconAltText = iconAltText;
	}

	/**
	 * @return the listColumns
	 */
	public String getListColumns() {
		return listColumns;
	}

	/**
	 * @param listColumns the listColumns to set
	 */
	public void setListColumns(String listColumns) {
		this.listColumns = listColumns;
	}
	
}
