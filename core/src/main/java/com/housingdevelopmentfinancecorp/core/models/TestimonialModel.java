package com.housingdevelopmentfinancecorp.core.models;

    public class TestimonialModel  {
    private String title;
    private String description;
    private String imagePath;
    private String altTitle;
    private String imageAltText;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getImagePath() {
            return imagePath;
        }

        public void setImagePath(String imagePath) {
            this.imagePath = imagePath;
        }

        public String getAltTitle() {
            return altTitle;
        }

        public void setAltTitle(String altTitle) {
            this.altTitle = altTitle;
        }

        public String getImageAltText() {
            return imageAltText;
        }

        public void setImageAltText(String imageAltText) {
            this.imageAltText = imageAltText;
        }
    }

