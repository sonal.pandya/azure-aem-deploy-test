package com.housingdevelopmentfinancecorp.core.models.impl;

import com.adobe.cq.wcm.core.components.commons.link.Link;
import com.adobe.cq.wcm.core.components.models.Image;
import com.adobe.cq.wcm.core.components.models.ListItem;
import com.adobe.cq.wcm.core.components.models.Teaser;
import com.housingdevelopmentfinancecorp.core.models.InvestInFDTeaserModel;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.factory.ModelFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Model(adapters = InvestInFDTeaserModel.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        adaptables = SlingHttpServletRequest.class,
        resourceType = InvestInFDTeaserModelImpl.RESOURCE_TYPE)
public class InvestInFDTeaserModelImpl implements InvestInFDTeaserModel {

    private Teaser teaser;
    private Image image;

    private List<Map<String,String>> bulletList;

    protected static final String RESOURCE_TYPE = "housingdevelopmentfinancecorp/components/homepage/invest-in-fd";

    @Inject
    Resource resource;

    @OSGiService
    private ModelFactory modelFactory;

    @Self
    private SlingHttpServletRequest request;

    @Override
    public List<Map<String, String>> getBulletList() {
        bulletList = new ArrayList<>();
        Resource articlestaxation = resource.getChild("");
        if (articlestaxation != null) {
            for (Resource articlestaxDetailsPartent : articlestaxation.getChildren()) {
                for (Resource articlestaxDetails : articlestaxDetailsPartent.getChildren()) {
                    Map<String, String> ariclestaxMap = new HashMap<>();
                    if(articlestaxDetails.getValueMap().get("bulletText", String.class) !=null) {
                        ariclestaxMap.put("bulletText", articlestaxDetails.getValueMap().get("bulletText", String.class));
                    }
                    bulletList.add(ariclestaxMap);
                }
            }
            return bulletList;
        }
        return bulletList;
    }

    @Override
    public boolean isActionsEnabled() {
        return teaser.isActionsEnabled();
    }

    @Override
    public List<ListItem> getActions() {
        return teaser.getActions();
    }

    @Override
    public Link getLink() {
        return teaser.getLink();
    }

    @Override
    public String getLinkURL() {
        return teaser.getLinkURL();
    }

    @Override
    public boolean isImageLinkHidden() {
        return teaser.isImageLinkHidden();
    }

    @Override
    public String getPretitle() {
        return teaser.getPretitle();
    }

    @Override
    public String getTitle() {
        return teaser.getTitle();
    }

    @Override
    public boolean isTitleLinkHidden() {
        return teaser.isTitleLinkHidden();
    }

    @Override
    public String getDescription() {
        return teaser.getDescription();
    }

    @Override
    public String getTitleType() {
        return teaser.getTitleType();
    }



    private Image getImage() {
        return image;
    }
    private Teaser getTeaser() {
        return teaser;
    }

    @PostConstruct
    void init(){
        teaser= modelFactory.getModelFromWrappedRequest(request,
                request.getResource(),Teaser.class);
        image = modelFactory.getModelFromWrappedRequest(request,
                request.getResource(),
                Image.class);
    }
}
