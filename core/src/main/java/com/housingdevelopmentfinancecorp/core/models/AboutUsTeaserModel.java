package com.housingdevelopmentfinancecorp.core.models;

import com.adobe.cq.wcm.core.components.commons.link.Link;
import com.adobe.cq.wcm.core.components.models.ListItem;
import org.apache.sling.api.resource.Resource;

import java.util.List;
import java.util.Map;

public interface AboutUsTeaserModel {

    String getDemo();
    List<Map<String,String>> getStats();

    boolean isActionsEnabled();
    List<ListItem> getActions();
    Link getLink();
    String getLinkURL();
//  Resource getImageResource();
    boolean isImageLinkHidden();
    String getPretitle() ;
    String getTitle();
    boolean isTitleLinkHidden();
    String getDescription();
    String getTitleType();
}
