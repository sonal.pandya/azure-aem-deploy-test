package com.housingdevelopmentfinancecorp.core.models;

import com.housingdevelopmentfinancecorp.core.services.ResourceHelper;
import com.housingdevelopmentfinancecorp.core.services.TestimonialDetailsService;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Model(adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class TestimoinialDetailsModel {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @OSGiService
    ResourceHelper resourceHelper;

    @OSGiService
    TestimonialDetailsService testimonialDetailsService;

    @Default(values = "")
    @Inject
    String searchpath;

    List<TestimonialModel> testimonialModels = new ArrayList<>();

    public List<TestimonialModel> getTestimonialModel() {
        return testimonialModels;
    }

    @PostConstruct
    public void activate(){
        logger.debug("TestimoinialDetailsModel activate Method Start :");
        try{

            testimonialModels=testimonialDetailsService.getTestimonialDetailsService(searchpath,resourceHelper);
        }catch (Exception e){
            logger.debug("TestimonialDetailsModel Exception :-"+e.toString());
        }
        logger.debug("TestimoinialDetailsModel activate Method End: ");
    }
}
