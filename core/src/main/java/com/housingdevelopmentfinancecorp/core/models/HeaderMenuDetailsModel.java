package com.housingdevelopmentfinancecorp.core.models;

/**
 * @author Naresh.Gounder
 *
 */

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;	
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.housingdevelopmentfinancecorp.core.services.HdfcHomeLoanLoggerService;
import com.housingdevelopmentfinancecorp.core.services.HeaderMenuDetailsService;
import com.housingdevelopmentfinancecorp.core.services.ResourceHelper;

@Model(adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class HeaderMenuDetailsModel {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Reference
	HdfcHomeLoanLoggerService hdfcHomeLoanLoggerService;

	@OSGiService
	ResourceHelper resourceHelper;

	@OSGiService
	HeaderMenuDetailsService headerLoanProductDetailsService;

	@Default(values = "")
	@Inject
	String searchpath;

	List<HeaderMenuModel> headerLoanProductModel = new ArrayList<>();

	/**
	 * @return the headerLoanProductModel
	 */
	public List<HeaderMenuModel> getHeaderMenuModel() {
		return headerLoanProductModel;
	}

	@PostConstruct
	public void activate() {
		logger.debug("HeaderLoanProductDetailsModel activate Method Start ");
		try {
			logger.debug("parameter: searchpath - " + searchpath);
			headerLoanProductModel = headerLoanProductDetailsService.getHeaderLoanProductDetails(searchpath, resourceHelper);

		} catch (Exception e) {
			logger.error("Excpetion : " + e.toString());
			hdfcHomeLoanLoggerService.loggerException(e);
		}
		logger.debug("HeaderLoanProductDetailsModel activate Method End");
	}

}
