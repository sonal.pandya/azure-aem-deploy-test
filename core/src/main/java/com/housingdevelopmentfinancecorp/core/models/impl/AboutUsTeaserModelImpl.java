package com.housingdevelopmentfinancecorp.core.models.impl;

import com.adobe.cq.wcm.core.components.commons.link.Link;
import com.adobe.cq.wcm.core.components.models.Image;
import com.adobe.cq.wcm.core.components.models.ListItem;
import com.adobe.cq.wcm.core.components.models.Search;
import com.adobe.cq.wcm.core.components.models.Teaser;
import com.housingdevelopmentfinancecorp.core.models.AboutUsTeaserModel;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.apache.sling.models.annotations.via.ResourceSuperType;
import org.apache.sling.models.factory.ModelFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.*;

@Model(adaptables = {SlingHttpServletRequest.class},
adapters = {AboutUsTeaserModel.class},
resourceType = AboutUsTeaserModelImpl.RESOURCE_TYPE,
defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL
)
public class AboutUsTeaserModelImpl implements AboutUsTeaserModel {

    protected static final String RESOURCE_TYPE = "housingdevelopmentfinancecorp/components/homepage/about-us";
    private Teaser teaser;
    private Image image;

    @Inject
    Resource resource;

    private List<Map<String,String>> stats;
    @ValueMapValue
    @Optional
    private String demo;

    @OSGiService
    private ModelFactory modelFactory;

    @Self
    private SlingHttpServletRequest request;


    @Override
    public String getDemo() {
        return demo;
    }

    @Override
    public String getDescription() {
        return teaser.getDescription();
    }

    @Override
    public String getTitleType() {
        return null;
    }

    @Override
    public String getTitle() {
        return teaser.getTitle();
    }

    @Override
    public boolean isTitleLinkHidden() {
        return false;
    }

    @Override
    public List<Map<String,String>> getStats() {
            stats = new ArrayList<>();
            Resource articlestaxation = resource.getChild("");
            if (articlestaxation != null) {
                for (Resource articlestaxDetailsPartent : articlestaxation.getChildren()) {
                    for (Resource articlestaxDetails : articlestaxDetailsPartent.getChildren()) {
                        Map<String, String> ariclestaxMap = new HashMap<>();
                        ariclestaxMap.put("statsParagraph", articlestaxDetails.getValueMap().get("statsParagraph", String.class));
                        ariclestaxMap.put("statsSubText", articlestaxDetails.getValueMap().get("statsSubText", String.class));
                        ariclestaxMap.put("statsValue", articlestaxDetails.getValueMap().get("statsValue", String.class));
                        stats.add(ariclestaxMap);
                    }
                }
                return stats;
            }
            return stats;
     }

    @Override
    public String getPretitle() {
        return teaser.getPretitle();
    }

    @Override
    public String getLinkURL() {
        return teaser.getLinkURL();
    }

    @Override
    public boolean isImageLinkHidden() {
        return teaser.isImageLinkHidden();
    }

    @Override
    public Link getLink() {
        return teaser.getLink();
    }

    @Override
    public boolean isActionsEnabled() {
        return teaser.isActionsEnabled();
    }

    @Override
    public List<ListItem> getActions() {
        return teaser.getActions();
    }

    private Image getImage() {
        return image;
    }
    private Teaser getTeaser() {
        return teaser;
    }


    @PostConstruct
    void init(){
        teaser = modelFactory.getModelFromWrappedRequest(request,
                request.getResource(),Teaser.class);
        image = modelFactory.getModelFromWrappedRequest(request,
                request.getResource(),
                Image.class);


//        teaser.getTitle();
    }
}
