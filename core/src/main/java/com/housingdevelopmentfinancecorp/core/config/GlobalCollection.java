package com.housingdevelopmentfinancecorp.core.config;

import org.apache.sling.caconfig.annotation.Configuration;
import org.apache.sling.caconfig.annotation.Property;

@Configuration(collection = true, label = "Global Variable Configuration Collection",description = "Collection of global Varable")
public @interface GlobalCollection {
    @Property(label="Key",description = "Key")
    String key();
    @Property(label="Value",description = "Value")
    String value();
}
