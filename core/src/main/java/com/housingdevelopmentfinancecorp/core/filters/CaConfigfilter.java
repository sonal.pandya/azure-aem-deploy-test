package com.housingdevelopmentfinancecorp.core.filters;


import com.housingdevelopmentfinancecorp.core.services.UrlRewriterService;
import com.housingdevelopmentfinancecorp.core.services.impl.HtmlResponseWrapper;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;

import org.apache.sling.engine.EngineConstants;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@Component(service = Filter.class,property = {
        Constants.SERVICE_DESCRIPTION+"= filter for Context aware Configuration",
        EngineConstants.SLING_FILTER_SCOPE+"="+EngineConstants.FILTER_SCOPE_REQUEST,
        Constants.SERVICE_RANKING+":Integer=-700",
        "sling.filter.extensions=html",
        EngineConstants.SLING_FILTER_PATTERN+"="+"/content/housingdevelopmentfinancecorp/.*"
})
public class CaConfigfilter implements Filter {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Reference
    UrlRewriterService service;


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException
    {
        final SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) request;
        response.setContentType("text/html; charset=UTF-8");
        response.setContentType("UTF-8");
        HtmlResponseWrapper responseWrapper = new HtmlResponseWrapper((HttpServletResponse) response);
        if(slingRequest.getRequestURI().contains("/configuration.html")){
            chain.doFilter(request, response);
        }else{
            chain.doFilter(request, responseWrapper);
        }

         if (!slingRequest.getRequestURI().contains("/configuration.html") && response.getContentType() != null && response.getContentType().contains("text/html")) {
             Document document = null;
             try {
                 ResourceResolver resourceResolver = slingRequest.getResourceResolver();
                 Resource resource = resourceResolver.getResource(slingRequest.getRequestPathInfo().getResourcePath());
                 String rewroteContent = service.rewriteUrl(responseWrapper.getString(), resource, slingRequest);
                 document = Jsoup.parse(rewroteContent);
                 response.setContentType("text/html;charset=UTF-8");
                 response.getWriter().write(document.toString());
             } catch (Exception e) {
                 logger.info("error -->", e.getMessage());
                 document = Jsoup.parse(responseWrapper.getString());
                 response.getWriter().write(document.toString());
             }
         }
     }


    @Override
    public void destroy() {

    }
}
